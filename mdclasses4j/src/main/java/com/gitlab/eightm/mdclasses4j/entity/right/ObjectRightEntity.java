/*
 * Copyright (c) 2022. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.entity.right;

import com.gitlab.eightm.mdclasses4j.internal.MdoReference;
import com.gitlab.eightm.mdclasses4j.jaxb.MdoReferenceAdapter;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.Data;

import java.util.List;

@Data
public class ObjectRightEntity {

  @XmlElement(namespace = "http://v8.1c.ru/8.2/roles")
  @XmlJavaTypeAdapter(MdoReferenceAdapter.class)
  private MdoReference name;

  @XmlElement(namespace = "http://v8.1c.ru/8.2/roles")
  private List<RightEntity> right;
}
