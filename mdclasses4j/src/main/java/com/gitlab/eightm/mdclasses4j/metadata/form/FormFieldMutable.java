/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.metadata.form;

import com.gltlab.eightm.mdclasses.model.base.MultiLanguageText;
import com.gltlab.eightm.mdclasses.model.base.RoleBoolean;
import com.gltlab.eightm.mdclasses.model.forms.EventHandler;
import com.gltlab.eightm.mdclasses.model.forms.ExtendedTooltip;
import com.gltlab.eightm.mdclasses.model.forms.element.ContextMenu;
import com.gltlab.eightm.mdclasses.model.forms.element.FormField;
import com.gltlab.eightm.mdclasses.model.forms.element.FormStandardCommand;
import com.gltlab.eightm.mdclasses.model.forms.ext.FieldExtInfo;
import com.gltlab.eightm.mdclasses.model.forms.path.GeneralDataPath;
import com.gltlab.eightm.mdclasses.model.v8enum.DisplayImportance;
import com.gltlab.eightm.mdclasses.model.v8enum.ElementTitleLocation;
import com.gltlab.eightm.mdclasses.model.v8enum.FormFixedInTable;
import com.gltlab.eightm.mdclasses.model.v8enum.ItemHorizontalAlignment;
import com.gltlab.eightm.mdclasses.model.v8enum.ItemVerticalAlignment;
import com.gltlab.eightm.mdclasses.model.v8enum.ManagedFormFieldType;
import com.gltlab.eightm.mdclasses.model.v8enum.OnMainServerUnavailableBehavior;
import com.gltlab.eightm.mdclasses.model.v8enum.TableFieldEditMode;
import com.gltlab.eightm.mdclasses.model.v8enum.TooltipRepresentation;
import com.gltlab.eightm.mdclasses.model.v8enum.WarningOnEditRepresentation;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Setter
@Getter
public class FormFieldMutable implements FormField {
  private String name;
  private String font;
  private List<EventHandler> handlers = new ArrayList<>();
  private TooltipRepresentation tooltipRepresentation;
  private MultiLanguageText tooltip;
  private boolean defaultItem;
  private boolean readOnly;
  private boolean enabled;
  private boolean visible;
  private RoleBoolean userVisible;
  private String shortcut;
  private boolean skipOnInput;
  private MultiLanguageText title;
  private ContextMenu contextMenu;
  private GeneralDataPath dataPath;
  private String titleBackColor;
  private int titleHeight;
  private ElementTitleLocation titleLocation;
  private int id;
  private DisplayImportance displayImportance;
  private List<FormStandardCommand> commands = new ArrayList<>();
  private List<FormStandardCommand> excludedCommands = new ArrayList<>();
  private ExtendedTooltip extendedTooltip;
  private FieldExtInfo extInfo;
  private ManagedFormFieldType type;
  private WarningOnEditRepresentation warningOnEditRepresentation;
  private Map<String, String> warningOnEdit = new HashMap<>();
  private TableFieldEditMode editMode;
  private FormFixedInTable fixingInTable;
  private boolean cellHyperlink;
  private boolean autoCellHeight;
  private boolean showInHeader;
  private ItemHorizontalAlignment headerHorizontalAlign;
  private boolean showInFooter;
  private GeneralDataPath footerDataPath;
  private Map<String, String> footerText = new HashMap<>();
  private ItemHorizontalAlignment footerHorizontalAlign;
  private OnMainServerUnavailableBehavior onMainServerUnavailableBehavior;
  private ItemHorizontalAlignment groupHorizontalAlignment;
  private ItemVerticalAlignment groupVerticalAlignment;
  private ItemHorizontalAlignment horizontalAlignment;
  private ItemVerticalAlignment verticalAlignment;
}
