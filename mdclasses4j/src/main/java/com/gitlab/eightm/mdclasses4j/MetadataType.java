/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j;

public enum MetadataType {
  SUBSYSTEM, STYLE, STYLE_ITEM, COMMON_PICTURE, SESSION_PARAMETER, ROLE,
  LANGUAGE, COMMON_TEMPLATE, FILTER_CRITERIA, COMMON_MODULE, COMMON_ATTRIBUTE, EXCHANGE_PLAN, XDTO_PACKAGE, WEB_SERVICE,
  HTTP_SERVICE, EVENT_SUBSCRIPTION, SCHEDULED_JOB, SETTINGS_STORAGE, FUNCTIONAL_OPTION, FUNCTIONAL_OPTION_PARAMETER,
  DEFINED_TYPE, COMMAND_COMMAND, COMMAND_GROUP, CONSTANT, COMMON_FORM, CATALOG, DOCUMENT, SEQUENCE, DOCUMENT_JOURNAL,
  ENUM, REPORT, DATA_PROCESSOR, INFORMATION_REGISTER, ACCUMULATION_REGISTER, CHART_OF_CHARACTERISTIC_TYPES, CHART_OF_ACCOUNTS,
  ACCOUNTING_REGISTER, CHART_OF_CALCULATION_TYPES, CALCULATION_REGISTER, BUSINESS_PROCESS, TASK;

  public static MetadataType getMetadataTypeByString(String value) {
    return switch (value) {
      case "Subsystem" -> SUBSYSTEM;
      case "StyleItem" -> STYLE_ITEM;
      case "CommonPicture" -> COMMON_PICTURE;
      case "SessionParameter" -> SESSION_PARAMETER;
      case "Role" -> ROLE;
      case "CommonTemplate" -> COMMON_TEMPLATE;
      case "FilterCriterion" -> FILTER_CRITERIA;
      case "CommonModule" -> COMMON_MODULE;
      case "CommonAttribute" -> COMMON_ATTRIBUTE;
      case "ExchangePlan" -> EXCHANGE_PLAN;
      case "XDTOPackage" -> XDTO_PACKAGE;
      case "WebService" -> WEB_SERVICE;
      case "HTTPService" -> HTTP_SERVICE;
      case "EventSubscription" -> EVENT_SUBSCRIPTION;
      case "ScheduledJob" -> SCHEDULED_JOB;
      case "SettingsStorage" -> SETTINGS_STORAGE;
      case "FunctionalOption" -> FUNCTIONAL_OPTION;
      case "FunctionalOptionsParameter" -> FUNCTIONAL_OPTION_PARAMETER;
      case "DefinedType" -> DEFINED_TYPE;
      case "CommonCommand" -> COMMAND_COMMAND;
      case "CommandGroup" -> COMMAND_GROUP;
      case "Constant" -> CONSTANT;
      case "CommonForm" -> COMMON_FORM;
      case "Catalog" -> CATALOG;
      case "Document" -> DOCUMENT;
      case "Sequence" -> SEQUENCE;
      case "DocumentJournal" -> DOCUMENT_JOURNAL;
      case "Enum" -> ENUM;
      case "Report" -> REPORT;
      case "DataProcessor" -> DATA_PROCESSOR;
      case "InformationRegister" -> INFORMATION_REGISTER;
      case "AccumulationRegister" -> ACCUMULATION_REGISTER;
      case "ChartOfCharacteristicTypes" -> CHART_OF_CHARACTERISTIC_TYPES;
      case "ChartOfAccounts" -> CHART_OF_ACCOUNTS;
      case "AccountingRegister" -> ACCOUNTING_REGISTER;
      case "ChartOfCalculationTypes" -> CHART_OF_CALCULATION_TYPES;
      case "CalculationRegister" -> CALCULATION_REGISTER;
      case "BusinessProcess" -> BUSINESS_PROCESS;
      case "Task" -> TASK;
      case "Language" -> LANGUAGE;
      default -> throw new IllegalArgumentException("Unknown metadata type " + value);
    };
  }
}
