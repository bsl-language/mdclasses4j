/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.metadata.general;

import com.gltlab.eightm.mdclasses.model.Configuration;
import com.gltlab.eightm.mdclasses.model.ContainedObject;
import com.gltlab.eightm.mdclasses.model.Help;
import com.gltlab.eightm.mdclasses.model.base.MultiLanguageText;
import com.gltlab.eightm.mdclasses.model.md.AccountingRegister;
import com.gltlab.eightm.mdclasses.model.md.AccumulationRegister;
import com.gltlab.eightm.mdclasses.model.md.Bot;
import com.gltlab.eightm.mdclasses.model.md.BusinessProcess;
import com.gltlab.eightm.mdclasses.model.md.CalculationRegister;
import com.gltlab.eightm.mdclasses.model.md.Catalog;
import com.gltlab.eightm.mdclasses.model.md.ChartOfAccounts;
import com.gltlab.eightm.mdclasses.model.md.ChartOfCalculationTypes;
import com.gltlab.eightm.mdclasses.model.md.ChartOfCharacteristicTypes;
import com.gltlab.eightm.mdclasses.model.md.CommandGroup;
import com.gltlab.eightm.mdclasses.model.md.CommonAttribute;
import com.gltlab.eightm.mdclasses.model.md.CommonCommand;
import com.gltlab.eightm.mdclasses.model.md.CommonForm;
import com.gltlab.eightm.mdclasses.model.md.CommonModule;
import com.gltlab.eightm.mdclasses.model.md.CommonTemplate;
import com.gltlab.eightm.mdclasses.model.md.Constant;
import com.gltlab.eightm.mdclasses.model.md.DataProcessor;
import com.gltlab.eightm.mdclasses.model.md.DefinedType;
import com.gltlab.eightm.mdclasses.model.md.Document;
import com.gltlab.eightm.mdclasses.model.md.DocumentJournal;
import com.gltlab.eightm.mdclasses.model.md.EventSubscription;
import com.gltlab.eightm.mdclasses.model.md.ExchangePlan;
import com.gltlab.eightm.mdclasses.model.md.ExternalDataSource;
import com.gltlab.eightm.mdclasses.model.md.FilterCriteria;
import com.gltlab.eightm.mdclasses.model.md.FunctionalOption;
import com.gltlab.eightm.mdclasses.model.md.HttpService;
import com.gltlab.eightm.mdclasses.model.md.InformationRegister;
import com.gltlab.eightm.mdclasses.model.md.IntegrationService;
import com.gltlab.eightm.mdclasses.model.md.Language;
import com.gltlab.eightm.mdclasses.model.md.Picture;
import com.gltlab.eightm.mdclasses.model.md.Report;
import com.gltlab.eightm.mdclasses.model.md.Role;
import com.gltlab.eightm.mdclasses.model.md.ScheduledJob;
import com.gltlab.eightm.mdclasses.model.md.SessionParameter;
import com.gltlab.eightm.mdclasses.model.md.SettingsStorage;
import com.gltlab.eightm.mdclasses.model.md.Style;
import com.gltlab.eightm.mdclasses.model.md.StyleItem;
import com.gltlab.eightm.mdclasses.model.md.Subsystem;
import com.gltlab.eightm.mdclasses.model.md.Task;
import com.gltlab.eightm.mdclasses.model.md.V8Enum;
import com.gltlab.eightm.mdclasses.model.md.V8Interface;
import com.gltlab.eightm.mdclasses.model.md.WebService;
import com.gltlab.eightm.mdclasses.model.md.WsReference;
import com.gltlab.eightm.mdclasses.model.md.XdtoPackage;
import com.gltlab.eightm.mdclasses.model.md.children.DocumentNumerator;
import com.gltlab.eightm.mdclasses.model.md.children.FunctionalOptionParameter;
import com.gltlab.eightm.mdclasses.model.md.children.Sequence;
import com.gltlab.eightm.mdclasses.model.md.children.UsedFunctionality;
import com.gltlab.eightm.mdclasses.model.modules.Module;
import com.gltlab.eightm.mdclasses.model.v8enum.ApplicationUsePurpose;
import com.gltlab.eightm.mdclasses.model.v8enum.ClientRunMode;
import com.gltlab.eightm.mdclasses.model.v8enum.CompatibilityMode;
import com.gltlab.eightm.mdclasses.model.v8enum.ConfigurationExtensionPurpose;
import com.gltlab.eightm.mdclasses.model.v8enum.DataLockControlMode;
import com.gltlab.eightm.mdclasses.model.v8enum.InterfaceCompatibilityMode;
import com.gltlab.eightm.mdclasses.model.v8enum.MainClientApplicationWindowMode;
import com.gltlab.eightm.mdclasses.model.v8enum.ModalityUseMode;
import com.gltlab.eightm.mdclasses.model.v8enum.ObjectAutoNumerationMode;
import com.gltlab.eightm.mdclasses.model.v8enum.RequiredMobileApplicationPermissions;
import com.gltlab.eightm.mdclasses.model.v8enum.ScriptVariant;
import com.gltlab.eightm.mdclasses.model.v8enum.SynchronousPlatformExtensionAndAddInCallUseMode;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class ConfigurationMutable {
  private List<ContainedObject> containedObjects = new ArrayList<>();
  private String namePrefix = "";
  private CompatibilityMode configurationCompatibilityMode;
  private ConfigurationExtensionPurpose configurationExtensionPurpose;
  private ClientRunMode defaultRunMode;
  private List<ApplicationUsePurpose> usePurposes = new ArrayList<>();
  private ScriptVariant scriptVariant;
  private Role defaultRole;
  private List<Role> defaultRoles = new ArrayList<>();
  private String vendor = "";
  private String version = "";
  private String updateCatalogAddress = "";
  private Module managedApplicationModule;
  private Module sessionModule;
  private Module externalConnectionModule;
  private Module ordinaryApplicationModule;
  private boolean includeHelpInContents;
  private Help help;
  private boolean useManagedFormInOrdinaryApplication;
  private boolean useOrdinaryFormInManagedApplication;
  private SettingsStorage commonSettingsStorage;
  private SettingsStorage reportsUserSettingsStorage;
  private SettingsStorage reportsVariantsStorage;
  private SettingsStorage formDataSettingsStorage;
  private SettingsStorage dynamicListsUserSettingsStorage;
  private CommonForm defaultReportForm;
  private CommonForm defaultReportVariantForm;
  private CommonForm defaultReportSettingsForm;
  private CommonForm defaultDynamicListSettingsForm;
  private CommonForm defaultSearchForm;
  private CommonForm defaultDataHistoryChangeHistoryForm;
  private CommonForm defaultDataHistoryVersionDataForm;
  private CommonForm defaultDataHistoryVersionDifferencesForm;
  private CommonForm defaultCollaborationSystemUsersChoiceForm;
  private List<RequiredMobileApplicationPermissions> requiredMobileApplicationPermissions = new ArrayList<>();
  private Language defaultLanguage;
  private MultiLanguageText briefInformation;
  private MultiLanguageText detailedInformation;
  private MultiLanguageText copyright;
  private MultiLanguageText vendorInformationAddress;
  private MultiLanguageText configurationInformationAddress;
  private MainClientApplicationWindowMode mainClientApplicationWindowMode;
  private DataLockControlMode dataLockControlMode = DataLockControlMode.MANAGED;
  private ObjectAutoNumerationMode objectAutoNumerationMode;
  private ModalityUseMode modalityUseMode;
  private SynchronousPlatformExtensionAndAddInCallUseMode synchronousPlatformExtensionAndAddInCallUseMode;
  private SynchronousPlatformExtensionAndAddInCallUseMode synchronousExtensionAndAddInCallUseMode;
  private InterfaceCompatibilityMode interfaceCompatibilityMode;
  private CompatibilityMode compatibilityMode;
  private CommonForm defaultConstantsForm;
  private List<Language> languages = new ArrayList<>();
  private List<Subsystem> subsystems = new ArrayList<>();
  private List<StyleItem> styleItems = new ArrayList<>();
  private List<Style> styles = new ArrayList<>();
  private List<Catalog> catalogs = new ArrayList<>();
  private String comment;
  private String name;
  private MultiLanguageText synonym;
  private UUID uuid;
  private Picture mainSectionPicture;
  private Picture splash;
  private Style defaultStyle;
  private List<Picture> commonPictures = new ArrayList<>();
  private List<V8Interface> interfaces = new ArrayList<>();
  private List<SessionParameter> sessionParameters = new ArrayList<>();
  private List<Role> roles = new ArrayList<>();
  private List<CommonTemplate> commonTemplates = new ArrayList<>();
  private List<FilterCriteria> filterCriteria = new ArrayList<>();
  private List<CommonModule> commonModules = new ArrayList<>();
  private List<CommonAttribute> commonAttributes = new ArrayList<>();
  private List<ExchangePlan> exchangePlans = new ArrayList<>();
  private List<XdtoPackage> xdtoPackages = new ArrayList<>();
  private List<WebService> webServices = new ArrayList<>();
  private List<HttpService> httpServices = new ArrayList<>();
  private List<WsReference> wsReferences = new ArrayList<>();
  private List<EventSubscription> eventSubscriptions = new ArrayList<>();
  private List<ScheduledJob> scheduledJobs = new ArrayList<>();
  private List<SettingsStorage> settingsStorages = new ArrayList<>();
  private List<FunctionalOption> functionalOptions = new ArrayList<>();
  private List<FunctionalOptionParameter> functionalOptionParameters = new ArrayList<>();
  private List<DefinedType> definedTypes = new ArrayList<>();
  private List<CommonCommand> commonCommands = new ArrayList<>();
  private List<CommandGroup> commandGroups = new ArrayList<>();
  private List<Constant> constants = new ArrayList<>();
  private List<CommonForm> commonForms = new ArrayList<>();
  private List<Document> documents = new ArrayList<>();
  private List<DocumentNumerator> documentNumerators = new ArrayList<>();
  private List<Sequence> sequences = new ArrayList<>();
  private List<DocumentJournal> documentJournals = new ArrayList<>();
  private List<V8Enum> enums = new ArrayList<>();
  private List<Report> reports = new ArrayList<>();
  private List<DataProcessor> dataProcessors = new ArrayList<>();
  private List<InformationRegister> informationRegisters = new ArrayList<>();
  private List<AccumulationRegister> accumulationRegisters = new ArrayList<>();
  private List<ChartOfCharacteristicTypes> chartsOfCharacteristicTypes = new ArrayList<>();
  private List<ChartOfAccounts> chartsOfAccounts = new ArrayList<>();
  private List<AccountingRegister> accountingRegisters = new ArrayList<>();
  private List<ChartOfCalculationTypes> chartsOfCalculationTypes = new ArrayList<>();
  private List<CalculationRegister> calculationRegisters = new ArrayList<>();
  private List<BusinessProcess> businessProcesses = new ArrayList<>();
  private List<Task> tasks = new ArrayList<>();
  private List<ExternalDataSource> externalDataSources = new ArrayList<>();
  private List<IntegrationService> integrationServices = new ArrayList<>();
  private List<Bot> bots = new ArrayList<>();
  private UsedFunctionality usedMobileApplicationFunctionalities;
}
