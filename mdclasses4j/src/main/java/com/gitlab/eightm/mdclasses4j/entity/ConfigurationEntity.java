/*
 * Copyright (c) 2022. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.entity;

import com.gitlab.eightm.mdclasses4j.internal.MdoReference;
import com.gitlab.eightm.mdclasses4j.jaxb.MdoReferenceAdapter;
import com.gitlab.eightm.mdclasses4j.metadata.form.MultiLanguageContentMutable;
import com.gitlab.eightm.mdclasses4j.metadata.general.children.ContainedObjectMutable;
import com.gltlab.eightm.mdclasses.model.ContainedObject;
import com.gltlab.eightm.mdclasses.model.base.MultiLanguageContent;
import com.gltlab.eightm.mdclasses.model.v8enum.ApplicationUsePurpose;
import com.gltlab.eightm.mdclasses.model.v8enum.ClientRunMode;
import com.gltlab.eightm.mdclasses.model.v8enum.CompatibilityMode;
import com.gltlab.eightm.mdclasses.model.v8enum.ScriptVariant;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.Data;

import java.util.List;

@XmlRootElement(name = "Configuration", namespace = "http://g5.1c.ru/v8/dt/metadata/mdclass")
@Data
public class ConfigurationEntity {
  @XmlAttribute
  private String uuid;

  @XmlElement
  private String name;

  @XmlElement(type = MultiLanguageContentMutable.class)
  private List<MultiLanguageContent> synonym;

  @XmlElement(type = ContainedObjectMutable.class)
  private List<ContainedObject> containedObjects;

  @XmlElement(name = "configurationExtensionCompatibilityMode")
  private CompatibilityMode configurationCompatibilityMode;

  @XmlElement
  private ClientRunMode defaultRunMode;

  @XmlElement
  private List<ApplicationUsePurpose> usePurposes;

  @XmlElement
  private ScriptVariant scriptVariant;

  @XmlElement
  @XmlJavaTypeAdapter(MdoReferenceAdapter.class)
  private List<MdoReference> defaultRoles;

  @XmlElement
  private String vendor;
}
