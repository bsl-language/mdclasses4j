/*
 * Copyright (c) 2021-2022. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.internal;

import com.gltlab.eightm.mdclasses.model.md.ConfigurationElement;
import com.gltlab.eightm.mdclasses.model.md.Role;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class MetadataIndex {
  private final Map<MdoReference, ConfigurationElement> index = new ConcurrentHashMap<>();

  public void add(@NotNull String elementReference, @NotNull ConfigurationElement configurationElement) {
    var mdoReference = new MdoReference(elementReference);
    add(mdoReference, configurationElement);
  }

  public void add(@NotNull MdoReference mdoReference, @NotNull ConfigurationElement configurationElement) {
    if (index.containsKey(mdoReference)) {
      throw new IllegalStateException(mdoReference + " already exists in metadata index");
    }
    index.put(mdoReference, configurationElement);
  }

  public Optional<ConfigurationElement> getByReference(@NotNull String elementReference) {
    var mdoReference = new MdoReference(elementReference);
    return Optional.ofNullable(index.get(mdoReference));
  }

  public Optional<ConfigurationElement> getByReference(@NotNull MdoReference mdoReference) {
    return Optional.ofNullable(index.get(mdoReference));
  }

  public Optional<Role> getRoleByReference(@NotNull MdoReference mdoReference) {
    return Optional.ofNullable(index.get(mdoReference))
      .filter(Role.class::isInstance)
      .map(Role.class::cast);
  }

}
