/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.metadata.form;

import com.gltlab.eightm.mdclasses.model.base.MultiLanguageText;
import com.gltlab.eightm.mdclasses.model.base.RoleBoolean;
import com.gltlab.eightm.mdclasses.model.forms.EventHandler;
import com.gltlab.eightm.mdclasses.model.forms.ExtendedTooltip;
import com.gltlab.eightm.mdclasses.model.forms.ext.DecorationExtInfo;
import com.gltlab.eightm.mdclasses.model.v8enum.DisplayImportance;
import com.gltlab.eightm.mdclasses.model.v8enum.FormDecorationType;
import com.gltlab.eightm.mdclasses.model.v8enum.ItemHorizontalAlignment;
import com.gltlab.eightm.mdclasses.model.v8enum.ItemVerticalAlignment;
import com.gltlab.eightm.mdclasses.model.v8enum.TooltipRepresentation;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class ExtendedTooltipMutable implements ExtendedTooltip {
  private int id;
  private FormDecorationType decorationType;
  private String name;
  private boolean visible;
  private boolean enabled;
  private RoleBoolean userVisible;
  private boolean autoMaxWidth;
  private boolean autoMaxHeight;
  private List<EventHandler> handlers = new ArrayList<>();
  private DecorationExtInfo extInfo;
  private boolean formatted;
  private TooltipRepresentation tooltipRepresentation;
  private MultiLanguageText tooltip;
  private boolean horizontalStretch;
  private boolean verticalStretch;
  private ItemHorizontalAlignment groupHorizontalAlignment;
  private ItemVerticalAlignment groupVerticalAlignment;
  private int height;
  private int maxHeight;
  private int maxWidth;
  private int minWidth;
  private String shortcut;
  private boolean skipOnInput;
  private MultiLanguageText title;
  private int width;
  private DisplayImportance displayImportance;
}
