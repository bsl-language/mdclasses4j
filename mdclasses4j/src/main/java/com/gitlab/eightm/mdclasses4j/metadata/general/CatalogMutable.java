/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.metadata.general;

import com.gltlab.eightm.mdclasses.model.Help;
import com.gltlab.eightm.mdclasses.model.StandardCommand;
import com.gltlab.eightm.mdclasses.model.base.MultiLanguageContent;
import com.gltlab.eightm.mdclasses.model.base.MultiLanguageText;
import com.gltlab.eightm.mdclasses.model.base.StandardAttribute;
import com.gltlab.eightm.mdclasses.model.forms.CatalogForm;
import com.gltlab.eightm.mdclasses.model.forms.element.FieldSource;
import com.gltlab.eightm.mdclasses.model.md.BasicConfigurationElement;
import com.gltlab.eightm.mdclasses.model.md.Catalog;
import com.gltlab.eightm.mdclasses.model.md.CatalogOwner;
import com.gltlab.eightm.mdclasses.model.md.CharacteristicsDescription;
import com.gltlab.eightm.mdclasses.model.md.children.CatalogTypes;
import com.gltlab.eightm.mdclasses.model.md.children.Field;
import com.gltlab.eightm.mdclasses.model.modules.ManagerModule;
import com.gltlab.eightm.mdclasses.model.modules.ObjectModule;
import com.gltlab.eightm.mdclasses.model.v8enum.AllowedLength;
import com.gltlab.eightm.mdclasses.model.v8enum.CatalogCodeType;
import com.gltlab.eightm.mdclasses.model.v8enum.CatalogMainPresentation;
import com.gltlab.eightm.mdclasses.model.v8enum.ChoiceMode;
import com.gltlab.eightm.mdclasses.model.v8enum.CreateOnInput;
import com.gltlab.eightm.mdclasses.model.v8enum.DataGetModeOnInputByString;
import com.gltlab.eightm.mdclasses.model.v8enum.DataLockControlMode;
import com.gltlab.eightm.mdclasses.model.v8enum.EditType;
import com.gltlab.eightm.mdclasses.model.v8enum.FullTextSearchOnInputByString;
import com.gltlab.eightm.mdclasses.model.v8enum.FullTextSearchUsing;
import com.gltlab.eightm.mdclasses.model.v8enum.HierarchyType;
import com.gltlab.eightm.mdclasses.model.v8enum.SearchStringModeOnInputByString;
import com.gltlab.eightm.mdclasses.model.v8enum.SubordinationUse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class CatalogMutable implements Catalog {
  private List<StandardCommand> commands = new ArrayList<>();
  private boolean hierarchical;
  private HierarchyType hierarchyType = HierarchyType.HIERARCHY_FOLDERS_AND_ITEMS;
  private boolean limitLevelCount;
  private int levelCount;
  private boolean foldersOnTop;
  private List<CatalogOwner> owners = new ArrayList<>();
  private SubordinationUse subordinationUse = SubordinationUse.TO_ITEMS;
  private int codeLength;
  private int descriptionLength;
  private List<CatalogForm> forms = new ArrayList<>();
  private CatalogForm defaultObjectForm;
  private CatalogForm defaultFolderForm;
  private CatalogForm defaultListForm;
  private CatalogForm defaultChoiceForm;
  private CatalogForm defaultFolderChoiceForm;
  private List<Field> fields = new ArrayList<>();
  private List<FieldSource> fieldSources = new ArrayList<>();
  private List<Field> inputByStringAttributes = new ArrayList<>();
  private FullTextSearchOnInputByString fullTextSearchOnInputByString;
  private SearchStringModeOnInputByString searchStringModeOnInputByString;
  private DataGetModeOnInputByString dataGetModeOnInputByString;
  private boolean useStandardCommands;
  private ObjectModule objectModule;
  private ManagerModule managerModule;
  private List<StandardAttribute> standardAttributes = new ArrayList<>();
  private List<CharacteristicsDescription> characteristics = new ArrayList<>();
  private List<BasicConfigurationElement> basedOn = new ArrayList<>();
  private List<Field> dataLockFields = new ArrayList<>();
  private DataLockControlMode dataLockControlMode;
  private CreateOnInput createOnInput;
  private boolean includeHelpInContents;
  private Help help;
  private FullTextSearchUsing fullTextSearch;
  private MultiLanguageText objectPresentation;
  private MultiLanguageText extendedObjectPresentation;
  private MultiLanguageText listPresentation;
  private MultiLanguageText extendedListPresentation;
  private MultiLanguageText explanation;
  private String comment;
  private String name;
  private List<MultiLanguageContent> synonym;
  private String uuid;
  private CatalogTypes producedTypes;
  private CatalogCodeType codeType;
  private AllowedLength codeAllowedLength;
  private boolean checkUnique;
  private boolean autoNumbering;
  private CatalogMainPresentation defaultPresentation;
  private EditType editType;
  private ChoiceMode choiceMode;
}
