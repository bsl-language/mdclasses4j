/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.metadata.form;

import com.gltlab.eightm.mdclasses.model.base.MultiLanguageText;
import com.gltlab.eightm.mdclasses.model.base.RoleBoolean;
import com.gltlab.eightm.mdclasses.model.forms.EventHandler;
import com.gltlab.eightm.mdclasses.model.forms.ExtendedTooltip;
import com.gltlab.eightm.mdclasses.model.forms.element.Addition;
import com.gltlab.eightm.mdclasses.model.forms.element.AutoCommandBar;
import com.gltlab.eightm.mdclasses.model.forms.element.ContextMenu;
import com.gltlab.eightm.mdclasses.model.forms.element.FieldSource;
import com.gltlab.eightm.mdclasses.model.forms.element.FormItem;
import com.gltlab.eightm.mdclasses.model.forms.element.FormStandardCommand;
import com.gltlab.eightm.mdclasses.model.forms.element.Table;
import com.gltlab.eightm.mdclasses.model.forms.ext.TableExtInfo;
import com.gltlab.eightm.mdclasses.model.forms.path.GeneralDataPath;
import com.gltlab.eightm.mdclasses.model.md.children.Field;
import com.gltlab.eightm.mdclasses.model.v8enum.CommandBarLocation;
import com.gltlab.eightm.mdclasses.model.v8enum.DataCompositionSettingsViewMode;
import com.gltlab.eightm.mdclasses.model.v8enum.DisplayImportance;
import com.gltlab.eightm.mdclasses.model.v8enum.ElementTitleLocation;
import com.gltlab.eightm.mdclasses.model.v8enum.FileDragMode;
import com.gltlab.eightm.mdclasses.model.v8enum.FormTableHeightControlVariant;
import com.gltlab.eightm.mdclasses.model.v8enum.ItemHorizontalAlignment;
import com.gltlab.eightm.mdclasses.model.v8enum.ItemVerticalAlignment;
import com.gltlab.eightm.mdclasses.model.v8enum.OnMainServerUnavailableBehavior;
import com.gltlab.eightm.mdclasses.model.v8enum.RefreshRequestMethod;
import com.gltlab.eightm.mdclasses.model.v8enum.SearchOnInput;
import com.gltlab.eightm.mdclasses.model.v8enum.TableBehaviorOnHorizontalCompression;
import com.gltlab.eightm.mdclasses.model.v8enum.TableCurrentRowUse;
import com.gltlab.eightm.mdclasses.model.v8enum.TableInitialListView;
import com.gltlab.eightm.mdclasses.model.v8enum.TableInitialTreeView;
import com.gltlab.eightm.mdclasses.model.v8enum.TableRepresentation;
import com.gltlab.eightm.mdclasses.model.v8enum.TableRowInputMode;
import com.gltlab.eightm.mdclasses.model.v8enum.TableRowSelectionMode;
import com.gltlab.eightm.mdclasses.model.v8enum.TableScrollBarUse;
import com.gltlab.eightm.mdclasses.model.v8enum.TableSelectionMode;
import com.gltlab.eightm.mdclasses.model.v8enum.TooltipRepresentation;
import com.gltlab.eightm.mdclasses.model.v8enum.UseOutput;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class TableMutable implements Table {
  private Addition viewStatusAddition;
  private Addition searchControlAddition;
  private Addition searchStringAddition;
  private List<FormItem> items = new ArrayList<>();
  private List<EventHandler> handlers = new ArrayList<>();
  private String name;
  private AutoCommandBar autoCommandBar;
  private CommandBarLocation commandBarLocation;
  private ExtendedTooltip extendedTooltip;
  private TooltipRepresentation tooltipRepresentation;
  private MultiLanguageText tooltip;
  private boolean defaultItem;
  private boolean enabled;
  private boolean skipOnInput;
  private MultiLanguageText title;
  private RoleBoolean userVisible;
  private boolean visible;
  private ContextMenu contextMenu;
  private GeneralDataPath dataPath;
  private String titleBackColor;
  private int titleHeight;
  private ElementTitleLocation titleLocation;
  private int id;
  private DisplayImportance displayImportance;
  private List<FormStandardCommand> commands;
  private List<FormStandardCommand> excludedCommands;
  private TableExtInfo extInfo;
  private boolean autoFill;
  private boolean autoMaxHeight;
  private boolean autoMaxWidth;
  private ItemHorizontalAlignment groupHorizontalAlignment;
  private ItemVerticalAlignment groupVerticalAlignment;
  private int height;
  private int width;
  private boolean horizontalStretch;
  private int maxHeight;
  private int maxWidth;
  private int minWidth;
  private boolean readOnly;
  private boolean verticalStretch;
  private TableRepresentation tableRepresentation;
  private boolean changeRowSet;
  private boolean changeRowOrder;
  private int heightInTableRows;
  private FormTableHeightControlVariant formFormTableHeightControlVariant;
  private boolean autoMaxRowsCount;
  private int maxRowsCount;
  private boolean choiceMode;
  private boolean multipleChoice;
  private TableRowInputMode tableRowInputMode;
  private TableSelectionMode tableSelectionMode;
  private TableRowSelectionMode tableRowSelectionMode;
  private boolean header;
  private int headerHeight;
  private boolean footer;
  private int footerHeight;
  private TableScrollBarUse horizontalScrollBarUse;
  private TableScrollBarUse verticalScrollBarUse;
  private boolean horizontalLines;
  private boolean verticalLines;
  private boolean useAlternationRowColor;
  private boolean autoInsertNewRow;
  private boolean autoAddIncomplete;
  private boolean autoMarkIncomplete;
  private SearchOnInput searchOnInput;
  private TableInitialListView initialListView;
  private TableInitialTreeView initialTreeView;
  private UseOutput output;
  private boolean enableStartDrag;
  private boolean enableDrag;
  private FileDragMode fileDragMode;
  private RefreshRequestMethod refreshRequestMethod;
  private TableCurrentRowUse currentRowUse;
  private TableBehaviorOnHorizontalCompression behaviorOnHorizontalCompression;
  private DataCompositionSettingsViewMode viewMode;
  private boolean settingsNamedItemDetailedRepresentation;
  private OnMainServerUnavailableBehavior onMainServerUnavailableBehavior;
  private List<Field> fields = new ArrayList<>();
  private List<FieldSource> fieldSources = new ArrayList<>();
}
