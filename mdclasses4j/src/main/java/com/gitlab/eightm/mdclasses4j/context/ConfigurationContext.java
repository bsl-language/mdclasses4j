/*
 * Copyright (c) 2022. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.context;

import com.gitlab.eightm.mdclasses4j.internal.ConfigurationParser;
import com.gitlab.eightm.mdclasses4j.internal.MdoReference;
import com.gitlab.eightm.mdclasses4j.internal.MetadataIndex;
import com.gitlab.eightm.mdclasses4j.entity.ConfigurationEntity;
import com.gitlab.eightm.mdclasses4j.jaxb.ConfigurationRoot;
import com.gltlab.eightm.mdclasses.model.md.Role;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ConfigurationContext {
  private ConfigurationRoot configurationRoot;
  private List<MdoReference> defaultRoles;
  private final MetadataIndex metadataIndex;
  private final ConfigurationParser parser;

  public ConfigurationContext(MetadataIndex metadataIndex, ConfigurationParser parser) {
    this.metadataIndex = metadataIndex;
    this.parser = parser;
  }

  public void initializeContext(ConfigurationEntity configurationEntity) {
    this.configurationRoot = new ConfigurationRoot(configurationEntity);
    this.defaultRoles = configurationEntity.defaultRoles();
  }

  public ConfigurationRoot getRoot() {
    return configurationRoot;
  }

  public List<Role> defaultRoles() {
    List<Role> result = new ArrayList<>();

    for (var defaultRoleReference : defaultRoles) {
      var role = metadataIndex.getRoleByReference(defaultRoleReference);
      if (role.isPresent()) {
        result.add(role.get());
      } else {
        parser.parseRole(defaultRoleReference);
      }
    }

    return result;
  }
}
