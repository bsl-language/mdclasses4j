/*
 * Copyright (c) 2022. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.jaxb;

import com.gitlab.eightm.mdclasses4j.ConfigurationModel;
import com.gitlab.eightm.mdclasses4j.metadata.form.MultiLanguageContentMutable;
import com.gltlab.eightm.mdclasses.model.base.MultiLanguageContent;
import com.gltlab.eightm.mdclasses.model.v8enum.ApplicationUsePurpose;
import com.gltlab.eightm.mdclasses.model.v8enum.ClientRunMode;
import com.gltlab.eightm.mdclasses.model.v8enum.CompatibilityMode;
import com.gltlab.eightm.mdclasses.model.v8enum.ScriptVariant;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

class ConfigurationRootTest {

  @SneakyThrows
  @Test
  void test() {
    var configurationModel = new ConfigurationModel();
    configurationModel.initializeContext(Path.of("../ssl_3_1/src"));
    var configurationRoot = configurationModel.getConfigurationRootData();

    assertThat(configurationRoot).isNotEmpty();
    var configurationRootData = configurationRoot.get();
    assertThat(configurationRootData.name()).isEqualTo("БиблиотекаСтандартныхПодсистемДемо");
    assertThat(configurationRootData.uuid()).isEqualTo("66193438-abc5-410b-a1f1-a204102d1a62");
    assertThat(configurationRootData.synonym()).hasSize(1);
    assertThat(configurationRootData.synonym().get(0)).isEqualTo(expectedSynonym());
    assertThat(configurationRootData.containedObjects()).hasSize(7);
    assertThat(configurationRootData.configurationCompatibilityMode()).isEqualTo(CompatibilityMode.V_8_3_18);
    assertThat(configurationRootData.defaultRunMode()).isEqualTo(ClientRunMode.MANAGED_APPLICATION);
    assertThat(configurationRootData.usePurposes()).hasSize(1);
    assertThat(configurationRootData.usePurposes().get(0)).isEqualTo(ApplicationUsePurpose.PERSONAL_COMPUTER);
    assertThat(configurationRootData.scriptVariant()).isEqualTo(ScriptVariant.RUSSIAN);


    configurationModel.getDefaultRoles();
  }

  private MultiLanguageContent expectedSynonym() {
    var content = new MultiLanguageContentMutable();
    content.languageCode("ru");
    content.content("Демонстрационная конфигурация \"Библиотека стандартных подсистем\", редакция 3.1");

    return content;
  }
}
