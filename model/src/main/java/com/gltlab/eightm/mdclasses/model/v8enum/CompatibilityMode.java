/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.v8enum;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum CompatibilityMode {
  @XmlEnumValue("8.3.5") V_8_3_5,
  @XmlEnumValue("8.3.6") V_8_3_6,
  @XmlEnumValue("8.3.7") V_8_3_7,
  @XmlEnumValue("8.3.8") V_8_3_8,
  @XmlEnumValue("8.3.9") V_8_3_9,
  @XmlEnumValue("8.3.10") V_8_3_10,
  @XmlEnumValue("8.3.11") V_8_3_11,
  @XmlEnumValue("8.3.12") V_8_3_12,
  @XmlEnumValue("8.3.13") V_8_3_13,
  @XmlEnumValue("8.3.13") V_8_3_14,
  @XmlEnumValue("8.3.15") V_8_3_15,
  @XmlEnumValue("8.3.16") V_8_3_16,
  @XmlEnumValue("8.3.17") V_8_3_17,
  @XmlEnumValue("8.3.18") V_8_3_18,
  @XmlEnumValue("8.3.19") V_8_3_19,
  @XmlEnumValue("8.3.20") V_8_3_20,
  @XmlEnumValue("8.3.21") V_8_3_21;

  public static CompatibilityMode getCompatibilityModeByString(String value) {
    return switch (value) {
      case "8.3.5" -> V_8_3_5;
      case "8.3.6" -> V_8_3_6;
      case "8.3.7" -> V_8_3_7;
      case "8.3.8" -> V_8_3_8;
      case "8.3.9" -> V_8_3_9;
      case "8.3.10" -> V_8_3_10;
      case "8.3.11" -> V_8_3_11;
      case "8.3.12" -> V_8_3_12;
      case "8.3.13" -> V_8_3_13;
      case "8.3.14" -> V_8_3_14;
      case "8.3.15" -> V_8_3_15;
      case "8.3.16" -> V_8_3_16;
      case "8.3.17" -> V_8_3_17;
      case "8.3.18" -> V_8_3_18;
      case "8.3.19" -> V_8_3_19;
      case "8.3.20" -> V_8_3_20;
      case "8.3.21" -> V_8_3_21;
      default -> throw new IllegalArgumentException("Unknown CompatibilityMode value " + value);
    };
  }
}
