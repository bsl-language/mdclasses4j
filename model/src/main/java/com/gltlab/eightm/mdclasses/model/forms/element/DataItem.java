/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.forms.element;

import com.gltlab.eightm.mdclasses.model.base.Nameable;
import com.gltlab.eightm.mdclasses.model.forms.base.DefaultItem;
import com.gltlab.eightm.mdclasses.model.forms.base.Display;
import com.gltlab.eightm.mdclasses.model.forms.base.SkipOnInput;
import com.gltlab.eightm.mdclasses.model.forms.base.Titled;
import com.gltlab.eightm.mdclasses.model.forms.path.GeneralDataPath;
import com.gltlab.eightm.mdclasses.model.v8enum.ElementTitleLocation;

public interface DataItem extends FormItem, Nameable, Titled, Display, SkipOnInput, DefaultItem {
  GeneralDataPath dataPath();

  void dataPath(GeneralDataPath dataPath);

  String titleBackColor(); // FIXME Color

  int titleHeight();

  ElementTitleLocation titleLocation();

  void titleLocation(ElementTitleLocation titleLocation);
}
