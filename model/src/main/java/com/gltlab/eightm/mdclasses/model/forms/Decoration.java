/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.forms;

import com.gltlab.eightm.mdclasses.model.base.Nameable;
import com.gltlab.eightm.mdclasses.model.forms.base.BaseAutoDimension;
import com.gltlab.eightm.mdclasses.model.forms.base.BaseDimension;
import com.gltlab.eightm.mdclasses.model.forms.base.BaseMaxDimension;
import com.gltlab.eightm.mdclasses.model.forms.base.Display;
import com.gltlab.eightm.mdclasses.model.forms.base.GroupsAlignment;
import com.gltlab.eightm.mdclasses.model.forms.base.MinWidthDimension;
import com.gltlab.eightm.mdclasses.model.forms.base.Shortcut;
import com.gltlab.eightm.mdclasses.model.forms.base.SkipOnInput;
import com.gltlab.eightm.mdclasses.model.forms.base.BaseStretchable;
import com.gltlab.eightm.mdclasses.model.forms.base.Titled;
import com.gltlab.eightm.mdclasses.model.forms.element.FormItem;
import com.gltlab.eightm.mdclasses.model.forms.ext.DecorationExtInfo;
import com.gltlab.eightm.mdclasses.model.forms.ext.ExtInfo;
import com.gltlab.eightm.mdclasses.model.v8enum.FormDecorationType;

public interface Decoration extends BaseDimension, BaseStretchable, Shortcut, ExtInfo, EventHandlerContainer, FormItem,
  Nameable, Titled, Display, TooltipContainer, SkipOnInput, GroupsAlignment, BaseAutoDimension, BaseMaxDimension, MinWidthDimension {
  boolean formatted();

  FormDecorationType decorationType();

  void decorationType(FormDecorationType decorationType);

  DecorationExtInfo extInfo();

  void extInfo(DecorationExtInfo extInfo);
}
