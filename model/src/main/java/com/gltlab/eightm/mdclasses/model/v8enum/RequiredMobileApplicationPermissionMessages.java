/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.v8enum;

public enum RequiredMobileApplicationPermissionMessages {
  AUDIO_PLAYBACK_AND_VIBRATION,
  AUTO_SEND_SMS,
  BACKGROUND_LOCATION,
  BIOMETRICS,
  BLUETOOTH_PRINTERS,
  CALENDARS,
  CALL_LOG,
  CALL_PROCESSING,
  CAMERA,
  CONTACTS,
  LOCATION,
  MICROPHONE,
  MUSIC_LIBRARY,
  NUMBER_DIALING,
  PICTURE_AND_VIDEO_LIBRARIES,
  RECEIVE_SMS,
  SMS_LOG;

  public static RequiredMobileApplicationPermissionMessages getRequiredMobileApplicationPermissionMessagesByString(String value) {
    return switch (value) {
      case "AudioPlaybackAndVibration" -> AUDIO_PLAYBACK_AND_VIBRATION;
      case "AutoSendSms" -> AUTO_SEND_SMS;
      case "BackgroundLocation" -> BACKGROUND_LOCATION;
      case "Biometrics" -> BIOMETRICS;
      case "BluetoothPrinters" -> BLUETOOTH_PRINTERS;
      case "Calendars" -> CALENDARS;
      case "CallLog" -> CALL_LOG;
      case "CallProcessing" -> CALL_PROCESSING;
      case "Camera" -> CAMERA;
      case "Contacts" -> CONTACTS;
      case "Location" -> LOCATION;
      case "Microphone" -> MICROPHONE;
      case "MusicLibrary" -> MUSIC_LIBRARY;
      case "NumberDialing" -> NUMBER_DIALING;
      case "PictureAndVideoLibraries" -> PICTURE_AND_VIDEO_LIBRARIES;
      case "ReceiveSms" -> RECEIVE_SMS;
      case "SmsLog" -> SMS_LOG;
      default -> throw new IllegalArgumentException("Unknown RequiredMobileApplicationPermissionMessages value " + value);
    };
  }
}
