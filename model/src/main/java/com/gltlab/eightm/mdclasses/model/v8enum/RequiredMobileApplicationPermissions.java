/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.v8enum;

public enum RequiredMobileApplicationPermissions {
  ADS,
  ALLOW_OS_BACKUP,
  AUDIO_AND_VIBRATION_PLAYBACK,
  BACKGROUND_AUDIO_PLAYBACK,
  BACKGROUND_LOCATION,
  CALENDARS,
  CALL_LOG,
  CALL_PHONE,
  CAMERA,
  CONTACTS,
  FILE_EXCHANGE_WITH_PERSONAL_COMPUTER,
  HANDLE_PHONE_CALLS,
  IN_APP_PURCHASES,
  INSTALL_PACKAGES,
  LOCAL_NOTIFICATION,
  LOCATION,
  MICROPHONE,
  MULTIMEDIA,
  MUSIC_LIBRARY,
  PERMISSION_GROUP_CALL_LOG,
  PERMISSION_GROUP_PHONE,
  PERMISSION_GROUP_SMS,
  PICTURES_AND_VIDEO_LIBRARIES,
  PRINT,
  PUSH_NOTIFICATION,
  RECEIVE_SMS,
  SEND_SMS,
  SMS_LOG,
  TELEPHONY;

  public static RequiredMobileApplicationPermissions getRequiredMobileApplicationPermissionsByString(String value) {
    return switch (value) {
      case "Ads" -> ADS;
      default -> throw new IllegalArgumentException("Unexpected RequiredMobileApplicationPermissions value " + value);
    };
  }
}
