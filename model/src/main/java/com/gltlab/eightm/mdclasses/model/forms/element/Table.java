/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.forms.element;

import com.gltlab.eightm.mdclasses.model.forms.CommandBarOwner;
import com.gltlab.eightm.mdclasses.model.forms.EventHandlerContainer;
import com.gltlab.eightm.mdclasses.model.forms.ExtendedTooltipOwner;
import com.gltlab.eightm.mdclasses.model.forms.FormItemContainer;
import com.gltlab.eightm.mdclasses.model.forms.TooltipContainer;
import com.gltlab.eightm.mdclasses.model.forms.base.AutoFill;
import com.gltlab.eightm.mdclasses.model.forms.base.BaseAutoDimension;
import com.gltlab.eightm.mdclasses.model.forms.base.BaseDimension;
import com.gltlab.eightm.mdclasses.model.forms.base.BaseMaxDimension;
import com.gltlab.eightm.mdclasses.model.forms.base.BaseStretchable;
import com.gltlab.eightm.mdclasses.model.forms.base.GroupsAlignment;
import com.gltlab.eightm.mdclasses.model.forms.base.MinWidthDimension;
import com.gltlab.eightm.mdclasses.model.forms.base.ReadOnly;
import com.gltlab.eightm.mdclasses.model.forms.ext.TableExtInfo;
import com.gltlab.eightm.mdclasses.model.v8enum.DataCompositionSettingsViewMode;
import com.gltlab.eightm.mdclasses.model.v8enum.FileDragMode;
import com.gltlab.eightm.mdclasses.model.v8enum.FormTableHeightControlVariant;
import com.gltlab.eightm.mdclasses.model.v8enum.OnMainServerUnavailableBehavior;
import com.gltlab.eightm.mdclasses.model.v8enum.RefreshRequestMethod;
import com.gltlab.eightm.mdclasses.model.v8enum.SearchOnInput;
import com.gltlab.eightm.mdclasses.model.v8enum.TableBehaviorOnHorizontalCompression;
import com.gltlab.eightm.mdclasses.model.v8enum.TableCurrentRowUse;
import com.gltlab.eightm.mdclasses.model.v8enum.TableInitialListView;
import com.gltlab.eightm.mdclasses.model.v8enum.TableInitialTreeView;
import com.gltlab.eightm.mdclasses.model.v8enum.TableRepresentation;
import com.gltlab.eightm.mdclasses.model.v8enum.TableRowInputMode;
import com.gltlab.eightm.mdclasses.model.v8enum.TableRowSelectionMode;
import com.gltlab.eightm.mdclasses.model.v8enum.TableScrollBarUse;
import com.gltlab.eightm.mdclasses.model.v8enum.TableSelectionMode;
import com.gltlab.eightm.mdclasses.model.v8enum.UseOutput;

public interface Table extends DataItem, FormStandardCommandSource, TooltipContainer, FormItemContainer, CommandBarOwner,
  EventHandlerContainer, ContextMenuOwner, AdditionSource, AdditionContainer, ExtendedTooltipOwner, FieldSource, AutoFill,
  ReadOnly, BaseDimension, BaseAutoDimension, BaseMaxDimension, MinWidthDimension, BaseStretchable, GroupsAlignment {
  TableExtInfo extInfo();

  void extInfo(TableExtInfo extInfo);

  TableRepresentation tableRepresentation();

  void tableRepresentation(TableRepresentation tableRepresentation);

  boolean changeRowSet();

  void changeRowSet(boolean changeRowSet);

  boolean changeRowOrder();

  void changeRowOrder(boolean changeRowOrder);

  int heightInTableRows();

  void heightInTableRows(int heightInTableRows);

  FormTableHeightControlVariant formFormTableHeightControlVariant();

  void formFormTableHeightControlVariant(FormTableHeightControlVariant formFormTableHeightControlVariant);

  boolean autoMaxRowsCount();

  void autoMaxRowsCount(boolean autoMaxRowsCount);

  int maxRowsCount();

  void maxRowsCount(int maxRowsCount);

  boolean choiceMode();

  void choiceMode(boolean choiceMode);

  boolean multipleChoice();

  void multipleChoice(boolean multipleChoice);

  TableRowInputMode tableRowInputMode();

  void tableRowInputMode(TableRowInputMode tableRowInputMode);

  TableSelectionMode tableSelectionMode();

  void tableSelectionMode(TableSelectionMode tableSelectionMode);

  TableRowSelectionMode tableRowSelectionMode();

  void tableRowSelectionMode(TableRowSelectionMode tableRowSelectionMode);

  boolean header();

  void header(boolean header);

  int headerHeight();

  void headerHeight(int headerHeight);

  boolean footer();

  void footer(boolean footer);

  int footerHeight();

  void footerHeight(int footerHeight);

  TableScrollBarUse horizontalScrollBarUse();

  void horizontalScrollBarUse(TableScrollBarUse horizontalScrollBarUse);

  TableScrollBarUse verticalScrollBarUse();

  void verticalScrollBarUse(TableScrollBarUse verticalScrollBarUse);

  boolean horizontalLines();

  void horizontalLines(boolean horizontalLines);

  boolean verticalLines();

  void verticalLines(boolean verticalLines);

  boolean useAlternationRowColor();

  void useAlternationRowColor(boolean useAlternationRowColor);

  boolean autoInsertNewRow();

  void autoInsertNewRow(boolean autoInsertNewRow);

  boolean autoAddIncomplete();

  void autoAddIncomplete(boolean autoAddIncomplete);

  boolean autoMarkIncomplete();

  void autoMarkIncomplete(boolean autoMarkIncomplete);

  SearchOnInput searchOnInput();

  void searchOnInput(SearchOnInput searchOnInput);

  TableInitialListView initialListView();

  void initialListView(TableInitialListView initialListView);

  TableInitialTreeView initialTreeView();

  void initialTreeView(TableInitialTreeView initialTreeView);

  UseOutput output();

  void output(UseOutput output);

  boolean enableStartDrag();

  void enableStartDrag(boolean enableStartDrag);

  boolean enableDrag();

  void enableDrag(boolean enableDrag);

  FileDragMode fileDragMode();

  void fileDragMode(FileDragMode fileDragMode);

  RefreshRequestMethod refreshRequestMethod();

  void refreshRequestMethod(RefreshRequestMethod refreshRequestMethod);

  TableCurrentRowUse currentRowUse();

  void currentRowUse(TableCurrentRowUse currentRowUse);

  TableBehaviorOnHorizontalCompression behaviorOnHorizontalCompression();

  void behaviorOnHorizontalCompression(TableBehaviorOnHorizontalCompression behaviorOnHorizontalCompression);

  DataCompositionSettingsViewMode viewMode();

  void viewMode(DataCompositionSettingsViewMode viewMode);

  boolean settingsNamedItemDetailedRepresentation();

  void settingsNamedItemDetailedRepresentation(boolean settingsNamedItemDetailedRepresentation);

  OnMainServerUnavailableBehavior onMainServerUnavailableBehavior();

  void onMainServerUnavailableBehavior(OnMainServerUnavailableBehavior onMainServerUnavailableBehavior);
}
