/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.forms;

import com.gltlab.eightm.mdclasses.model.base.BasicCharacteristics;
import com.gltlab.eightm.mdclasses.model.forms.base.BaseDimension;
import com.gltlab.eightm.mdclasses.model.forms.base.BaseItemSpacing;
import com.gltlab.eightm.mdclasses.model.forms.base.ChildrenAlign;
import com.gltlab.eightm.mdclasses.model.forms.base.ChildrenGroup;
import com.gltlab.eightm.mdclasses.model.forms.base.EnableAbility;
import com.gltlab.eightm.mdclasses.model.forms.base.ItemsAlignment;
import com.gltlab.eightm.mdclasses.model.forms.base.ShowTitle;
import com.gltlab.eightm.mdclasses.model.forms.base.Titled;
import com.gltlab.eightm.mdclasses.model.forms.element.FormAttribute;
import com.gltlab.eightm.mdclasses.model.help.HelpContent;
import com.gltlab.eightm.mdclasses.model.v8enum.AutoSaveFormDataInSettings;
import com.gltlab.eightm.mdclasses.model.v8enum.CollapseFormItemsByImportance;
import com.gltlab.eightm.mdclasses.model.v8enum.FormBaseFontVariant;
import com.gltlab.eightm.mdclasses.model.v8enum.FormConversationsRepresentation;
import com.gltlab.eightm.mdclasses.model.v8enum.FormEnterKeyBehavior;
import com.gltlab.eightm.mdclasses.model.v8enum.FormType;
import com.gltlab.eightm.mdclasses.model.v8enum.FormWindowOpeningMode;
import com.gltlab.eightm.mdclasses.model.v8enum.LogFormScrollMode;
import com.gltlab.eightm.mdclasses.model.v8enum.SaveFormDataInSettings;

import java.util.List;

public interface Form extends BasicCharacteristics, BaseDimension, HelpContent, FormItemContainer,
  CommandBarOwner, EventHandlerContainer, EnableAbility, ChildrenGroup, ShowTitle, ChildrenAlign, BaseItemSpacing,
  ItemsAlignment, Titled {

  /**
   * @return Автоматическое сохранение данных формы в настройках
   */
  AutoSaveFormDataInSettings autoSaveFormDataInSettings();

  void autoSaveFormDataInSettings(AutoSaveFormDataInSettings autoSaveFormDataInSettings);

  /**
   * @return Сохранение данных формы в настройках
   */
  SaveFormDataInSettings saveFormDataInSettings();

  void saveFormDataInSettings(SaveFormDataInSettings saveFormDataInSettings);

  /**
   * @return Режим открытия окна
   */
  FormWindowOpeningMode windowOpeningMode();

  /**
   * @return Поведение клавиши enter
   */
  FormEnterKeyBehavior enterKeyBehavior();

  /**
   * @return Вариант масштаба
   */
  FormBaseFontVariant scalingMode();

  /**
   * @return Масштаб
   */
  double scale();

  /**
   * @return Вертикальная прокрутка
   */
  LogFormScrollMode verticalScroll();

  FormType formType();

  List<Event> formEvents();

  List<FormAttribute> attributes();

  boolean autoTitle();

  void autoTitle(boolean autoTitle);

  boolean autoUrl();

  void autoUrl(boolean autoUrl);

  boolean showCloseButton();

  void showCloseButton(boolean showCloseButton);

  boolean autoFillCheck();

  void autoFillCheck(boolean autoFillCheck);

  boolean allowFormCustomize();

  void allowFormCustomize(boolean allowFormCustomize);

  FormCommandInterface commandInterface();

  void commandInterface(FormCommandInterface commandInterface);

  void enterKeyBehavior(FormEnterKeyBehavior enterKeyBehavior);

  void windowOpeningMode(FormWindowOpeningMode windowOpeningMode);

  void scalingMode(FormBaseFontVariant scalingMode);

  void scale(double scale);

  void verticalScroll(LogFormScrollMode verticalScroll);

  String settingsStorage();

  void settingsStorage(String settingsStorage);

  FormConversationsRepresentation conversationsRepresentation();

  void conversationsRepresentation(FormConversationsRepresentation conversationsRepresentation);

  CollapseFormItemsByImportance collapseItemsByImportanceVariant();

  void collapseItemsByImportanceVariant(CollapseFormItemsByImportance collapseItemsByImportanceVariant);
}
