/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.md;

import com.gltlab.eightm.mdclasses.model.Help;
import com.gltlab.eightm.mdclasses.model.StandardCommandSource;
import com.gltlab.eightm.mdclasses.model.base.MultiLanguageText;
import com.gltlab.eightm.mdclasses.model.base.StandardAttribute;
import com.gltlab.eightm.mdclasses.model.forms.element.FieldSource;
import com.gltlab.eightm.mdclasses.model.help.HelpContent;
import com.gltlab.eightm.mdclasses.model.input.InputByString;
import com.gltlab.eightm.mdclasses.model.md.children.Field;
import com.gltlab.eightm.mdclasses.model.modules.ManagerModule;
import com.gltlab.eightm.mdclasses.model.modules.ObjectModule;
import com.gltlab.eightm.mdclasses.model.v8enum.CreateOnInput;
import com.gltlab.eightm.mdclasses.model.v8enum.DataLockControlMode;
import com.gltlab.eightm.mdclasses.model.v8enum.FullTextSearchUsing;

import java.util.List;

public interface BasicConfigurationElement extends FieldSource, ConfigurationElement, StandardCommandSource, InputByString, HelpContent {
  boolean useStandardCommands();

  void useStandardCommands(boolean useStandardCommands);

  ObjectModule objectModule();

  void objectModule(ObjectModule objectModule);

  ManagerModule managerModule();

  void managerModule(ManagerModule managerModule);

  List<StandardAttribute> standardAttributes();

  List<CharacteristicsDescription> characteristics();

  List<BasicConfigurationElement> basedOn();

  List<Field> dataLockFields();

  DataLockControlMode dataLockControlMode();

  void dataLockControlMode(DataLockControlMode dataLockControlMode);

  CreateOnInput createOnInput();

  void createOnInput(CreateOnInput createOnInput);

  Help help();

  void help(Help help);

  FullTextSearchUsing fullTextSearch();

  void fullTextSearch(FullTextSearchUsing fullTextSearch);

  MultiLanguageText objectPresentation();

  void objectPresentation(MultiLanguageText objectPresentation);

  MultiLanguageText extendedObjectPresentation();

  void extendedObjectPresentation(MultiLanguageText extendedObjectPresentation);

  MultiLanguageText listPresentation();

  void listPresentation(MultiLanguageText listPresentation);

  MultiLanguageText extendedListPresentation();

  void extendedListPresentation(MultiLanguageText extendedListPresentation);

  MultiLanguageText explanation();

  void explanation(MultiLanguageText explanation);
}
