/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.forms.element;

import com.gltlab.eightm.mdclasses.model.forms.ExtendedTooltipOwner;
import com.gltlab.eightm.mdclasses.model.forms.FormItemContainer;
import com.gltlab.eightm.mdclasses.model.forms.TooltipContainer;
import com.gltlab.eightm.mdclasses.model.forms.base.Display;
import com.gltlab.eightm.mdclasses.model.forms.base.GroupsAlignment;
import com.gltlab.eightm.mdclasses.model.forms.base.Titled;
import com.gltlab.eightm.mdclasses.model.forms.ext.AdditionExtInfo;
import com.gltlab.eightm.mdclasses.model.v8enum.FormAdditionType;

public interface Addition extends Titled, Display, TooltipContainer, FormItem, FormItemContainer, ContextMenuOwner, ExtendedTooltipOwner, GroupsAlignment {
  AdditionExtInfo extInfo();

  void extInfo(AdditionExtInfo extInfo);

  String source();

  void source(String source);

  FormAdditionType additionType();

  void additionType(FormAdditionType additionType);
}
