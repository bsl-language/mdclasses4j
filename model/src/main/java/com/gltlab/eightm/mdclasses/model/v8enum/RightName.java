/*
 * Copyright (c) 2022. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.v8enum;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum RightName {
  @XmlEnumValue("ActiveUsers") ACTIVE_USERS,
  @XmlEnumValue("Administration") ADMINISTRATION,
  @XmlEnumValue("AllFunctionsMode") ALL_FUNCTIONS_MODE,
  @XmlEnumValue("AnalyticsSystemClient") ANALYTICS_SYSTEM_CLIENT,
  @XmlEnumValue("Automation") AUTOMATION,
  @XmlEnumValue("CollaborationSystemInfoBaseRegistration") COLLABORATION_SYSTEM_INFO_BASE_REGISTRATION,
  @XmlEnumValue("ConfigurationExtensionsAdministration") CONFIGURATION_EXTENSIONS_ADMINISTRATION,
  @XmlEnumValue("DataAdministration") DATA_ADMINISTRATION,
  @XmlEnumValue("Delete") DELETE,
  @XmlEnumValue("Edit") EDIT,
  @XmlEnumValue("EditDataHistoryVersionComment") EDIT_DATA_HISTORY_VERSION_COMMENT,
  @XmlEnumValue("EventLog") EVENT_LOG,
  @XmlEnumValue("ExclusiveMode") EXCLUSIVE_MODE,
  @XmlEnumValue("ExclusiveModeTerminationAtSessionStart") EXCLUSIVE_MODE_TERMINATION_AT_SESSION_START,
  @XmlEnumValue("Execute") EXECUTE,
  @XmlEnumValue("ExternalConnection") EXTERNAL_CONNECTION,
  @XmlEnumValue("Get") GET,
  @XmlEnumValue("InputByString") INPUT_BY_STRING,
  @XmlEnumValue("Insert") INSERT,
  @XmlEnumValue("InteractiveDelete") INTERACTIVE_ACTIVATE,
  @XmlEnumValue("InteractiveChangeOfPosted") INTERACTIVE_CHANGE_OF_POSTED,
  @XmlEnumValue("InteractiveClearDeletionMark") INTERACTIVE_CLEAR_DELETION_MARK,
  @XmlEnumValue("InteractiveClearDeletionMarkPredefinedData") INTERACTIVE_CLEAR_DELETION_MARK_PREDEFINED_DATA,
  @XmlEnumValue("InteractiveDelete") INTERACTIVE_DELETE,
  @XmlEnumValue("InteractiveDeleteMarked") INTERACTIVE_DELETE_MARKED,
  @XmlEnumValue("InteractiveDeleteMarkedPredefinedData") INTERACTIVE_DELETE_MARKED_PREDEFINED_DATA,
  @XmlEnumValue("InteractiveDeletePredefinedData") INTERACTIVE_DELETE_PREDEFINED_DATA,
  @XmlEnumValue("InteractiveExecute") INTERACTIVE_EXECUTE,
  @XmlEnumValue("InteractiveInsert") INTERACTIVE_INSERT,
  @XmlEnumValue("InteractiveOpenExtDataProcessors") INTERACTIVE_OPEN_EXT_DATA_PROCESSORS,
  @XmlEnumValue("InteractiveOpenExtReports") INTERACTIVE_OPEN_EXT_REPORTS,
  @XmlEnumValue("InteractivePosting") INTERACTIVE_POSTING,
  @XmlEnumValue("InteractivePostingRegular") INTERACTIVE_POSTING_REGULAR,
  @XmlEnumValue("InteractiveSetDeletionMark") INTERACTIVE_SET_DELETION_MARK,
  @XmlEnumValue("InteractiveSetDeletionMarkPredefinedData") INTERACTIVE_SET_DELETION_MARK_PREDEFINED_DATA,
  @XmlEnumValue("InteractiveStart") INTERACTIVE_START,
  @XmlEnumValue("InteractiveUndoPosting") INTERACTIVE_UNDO_POSTING,
  @XmlEnumValue("MainWindowModeEmbeddedWorkplace") MAIN_WINDOW_MODE_EMBEDDED_WORKPLACE,
  @XmlEnumValue("MainWindowModeFullscreenWorkplace") MAIN_WINDOW_MODE_FULLSCREEN_WORKPLACE,
  @XmlEnumValue("MainWindowModeKiosk") MAIN_WINDOW_MODE_KIOSK,
  @XmlEnumValue("MainWindowModeNormal") MAIN_WINDOW_MODE_NORMAL,
  @XmlEnumValue("MainWindowModeWorkplace") MAIN_WINDOW_MODE_WORKPLACE,
  @XmlEnumValue("MobileClient") MOBILE_CLIENT,
  @XmlEnumValue("Output") OUTPUT,
  @XmlEnumValue("Posting") POSTING,
  @XmlEnumValue("Read") READ,
  @XmlEnumValue("ReadDataHistory") READ_DATA_HISTORY,
  @XmlEnumValue("ReadDataHistoryOfMissingData") READ_DATA_HISTORY_OF_MISSING_DATA,
  @XmlEnumValue("SaveUserData") SAVE_USER_DATA,
  @XmlEnumValue("SessionOsAuthenticationChange") SESSION_OS_AUTHENTICATION_CHANGE,
  @XmlEnumValue("SessionStandardAuthenticationChange") SESSION_STANDARD_AUTHENTICATION_CHANGE,
  @XmlEnumValue("Set") SET,
  @XmlEnumValue("StandardAuthenticationChange") STANDARD_AUTHENTICATION_CHANGE,
  @XmlEnumValue("Start") START,
  @XmlEnumValue("SwitchToDataHistoryVersion") SWITCH_TO_DATA_HISTORY_VERSION,
  @XmlEnumValue("TecnicalSpecialistMode") TECHNICAL_SPECIALIST_MODE,
  @XmlEnumValue("ThickClient") THICK_CLIENT,
  @XmlEnumValue("ThinClient") THIN_CLIENT,
  @XmlEnumValue("TotalsControl") TOTALS_CONTROL,
  @XmlEnumValue("UndoPosting") UNDO_POSTING,
  @XmlEnumValue("Update") UPDATE,
  @XmlEnumValue("UpdateDataBaseConfiguration") UPDATE_DATA_BASE_CONFIGURATION,
  @XmlEnumValue("UpdateDataHistory") UPDATE_DATA_HISTORY,
  @XmlEnumValue("UpdateDataHistoryOfMissingData") UPDATE_DATA_HISTORY_OF_MISSING_DATA,
  @XmlEnumValue("UpdateDataHistorySettings") UPDATE_DATA_HISTORY_SETTINGS,
  @XmlEnumValue("UpdateDataHistoryVersionComment") UPDATE_DATA_HISTORY_VERSION_COMMENT,
  @XmlEnumValue("Use") USE,
  @XmlEnumValue("View") VIEW,
  @XmlEnumValue("ViewDataHistory") VIEW_DATA_HISTORY,
  @XmlEnumValue("WebClient") WEB_CLIENT
}
