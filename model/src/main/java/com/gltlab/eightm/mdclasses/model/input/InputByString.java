/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.input;

import com.gltlab.eightm.mdclasses.model.md.children.Field;
import com.gltlab.eightm.mdclasses.model.v8enum.DataGetModeOnInputByString;
import com.gltlab.eightm.mdclasses.model.v8enum.FullTextSearchOnInputByString;
import com.gltlab.eightm.mdclasses.model.v8enum.SearchStringModeOnInputByString;

import java.util.List;

public interface InputByString {

  // Поля ввода по строке
  List<Field> inputByStringAttributes();

  // полнотекстовый поиск при вводе по строке
  FullTextSearchOnInputByString fullTextSearchOnInputByString();

  // Способ поиска строки
  SearchStringModeOnInputByString searchStringModeOnInputByString();

  // Режим получения данных выбора
  DataGetModeOnInputByString dataGetModeOnInputByString();

  void fullTextSearchOnInputByString(FullTextSearchOnInputByString fullTextSearchOnInputByString);
}
