/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model;

import com.gltlab.eightm.mdclasses.model.help.HelpContent;
import com.gltlab.eightm.mdclasses.model.md.AccountingRegister;
import com.gltlab.eightm.mdclasses.model.md.AccumulationRegister;
import com.gltlab.eightm.mdclasses.model.md.Bot;
import com.gltlab.eightm.mdclasses.model.md.BusinessProcess;
import com.gltlab.eightm.mdclasses.model.md.CalculationRegister;
import com.gltlab.eightm.mdclasses.model.md.Catalog;
import com.gltlab.eightm.mdclasses.model.base.Commentable;
import com.gltlab.eightm.mdclasses.model.base.MultiLanguageText;
import com.gltlab.eightm.mdclasses.model.base.Nameable;
import com.gltlab.eightm.mdclasses.model.base.SynonymOwner;
import com.gltlab.eightm.mdclasses.model.base.UUIDable;
import com.gltlab.eightm.mdclasses.model.md.ChartOfAccounts;
import com.gltlab.eightm.mdclasses.model.md.ChartOfCalculationTypes;
import com.gltlab.eightm.mdclasses.model.md.ChartOfCharacteristicTypes;
import com.gltlab.eightm.mdclasses.model.md.CommandGroup;
import com.gltlab.eightm.mdclasses.model.md.CommonAttribute;
import com.gltlab.eightm.mdclasses.model.md.CommonCommand;
import com.gltlab.eightm.mdclasses.model.md.CommonForm;
import com.gltlab.eightm.mdclasses.model.md.CommonModule;
import com.gltlab.eightm.mdclasses.model.md.CommonTemplate;
import com.gltlab.eightm.mdclasses.model.md.ConfigurationElement;
import com.gltlab.eightm.mdclasses.model.md.Constant;
import com.gltlab.eightm.mdclasses.model.md.DataProcessor;
import com.gltlab.eightm.mdclasses.model.md.DefinedType;
import com.gltlab.eightm.mdclasses.model.md.Document;
import com.gltlab.eightm.mdclasses.model.md.DocumentJournal;
import com.gltlab.eightm.mdclasses.model.md.EventSubscription;
import com.gltlab.eightm.mdclasses.model.md.ExchangePlan;
import com.gltlab.eightm.mdclasses.model.md.ExternalDataSource;
import com.gltlab.eightm.mdclasses.model.md.FilterCriteria;
import com.gltlab.eightm.mdclasses.model.md.FunctionalOption;
import com.gltlab.eightm.mdclasses.model.md.HttpService;
import com.gltlab.eightm.mdclasses.model.md.InformationRegister;
import com.gltlab.eightm.mdclasses.model.md.IntegrationService;
import com.gltlab.eightm.mdclasses.model.md.Language;
import com.gltlab.eightm.mdclasses.model.md.Picture;
import com.gltlab.eightm.mdclasses.model.md.Report;
import com.gltlab.eightm.mdclasses.model.md.Role;
import com.gltlab.eightm.mdclasses.model.md.ScheduledJob;
import com.gltlab.eightm.mdclasses.model.md.SessionParameter;
import com.gltlab.eightm.mdclasses.model.md.SettingsStorage;
import com.gltlab.eightm.mdclasses.model.md.Style;
import com.gltlab.eightm.mdclasses.model.md.StyleItem;
import com.gltlab.eightm.mdclasses.model.md.Subsystem;
import com.gltlab.eightm.mdclasses.model.md.Task;
import com.gltlab.eightm.mdclasses.model.md.V8Enum;
import com.gltlab.eightm.mdclasses.model.md.V8Interface;
import com.gltlab.eightm.mdclasses.model.md.WebService;
import com.gltlab.eightm.mdclasses.model.md.WsReference;
import com.gltlab.eightm.mdclasses.model.md.XdtoPackage;
import com.gltlab.eightm.mdclasses.model.md.children.DocumentNumerator;
import com.gltlab.eightm.mdclasses.model.md.children.FunctionalOptionParameter;
import com.gltlab.eightm.mdclasses.model.md.children.Sequence;
import com.gltlab.eightm.mdclasses.model.md.children.UsedFunctionality;
import com.gltlab.eightm.mdclasses.model.modules.Module;
import com.gltlab.eightm.mdclasses.model.v8enum.ApplicationUsePurpose;
import com.gltlab.eightm.mdclasses.model.v8enum.ClientRunMode;
import com.gltlab.eightm.mdclasses.model.v8enum.CompatibilityMode;
import com.gltlab.eightm.mdclasses.model.v8enum.ConfigurationExtensionPurpose;
import com.gltlab.eightm.mdclasses.model.v8enum.DataLockControlMode;
import com.gltlab.eightm.mdclasses.model.v8enum.InterfaceCompatibilityMode;
import com.gltlab.eightm.mdclasses.model.v8enum.MainClientApplicationWindowMode;
import com.gltlab.eightm.mdclasses.model.v8enum.ModalityUseMode;
import com.gltlab.eightm.mdclasses.model.v8enum.ObjectAutoNumerationMode;
import com.gltlab.eightm.mdclasses.model.v8enum.RequiredMobileApplicationPermissions;
import com.gltlab.eightm.mdclasses.model.v8enum.ScriptVariant;
import com.gltlab.eightm.mdclasses.model.v8enum.SynchronousPlatformExtensionAndAddInCallUseMode;

import java.util.List;

public interface Configuration extends ConfigurationElement, Nameable, UUIDable, SynonymOwner, Commentable, HelpContent {
  List<ContainedObject> containedObjects();

  String namePrefix();

  void namePrefix(String namePrefix);

  CompatibilityMode configurationCompatibilityMode();

  void configurationCompatibilityMode(CompatibilityMode configurationCompatibilityMode);

  ConfigurationExtensionPurpose configurationExtensionPurpose();

  void configurationExtensionPurpose(ConfigurationExtensionPurpose configurationExtensionPurpose);

  ClientRunMode defaultRunMode();

  void defaultRunMode(ClientRunMode defaultRunMode);

  List<ApplicationUsePurpose> usePurposes();

  ScriptVariant scriptVariant();

  void scriptVariant(ScriptVariant scriptVariant);

  Role defaultRole();

  void defaultRole(Role defaultRole);

  List<Role> defaultRoles();

  String vendor();

  void vendor(String vendor);

  String version();

  void version(String version);

  String updateCatalogAddress();

  void updateCatalogAddress(String updateCatalogAddress);

  Module managedApplicationModule();

  void managedApplicationModule(Module managedApplicationModule);

  Module sessionModule();

  void sessionModule(Module sessionModule);

  Module externalConnectionModule();

  void externalConnectionModule(Module externalConnectionModule);

  Module ordinaryApplicationModule();

  void ordinaryApplicationModule(Module ordinaryApplicationModule);

  boolean useManagedFormInOrdinaryApplication();

  void useManagedFormInOrdinaryApplication(boolean useManagedFormInOrdinaryApplication);

  boolean useOrdinaryFormInManagedApplication();

  void useOrdinaryFormInManagedApplication(boolean useOrdinaryFormInManagedApplication);

  SettingsStorage commonSettingsStorage();

  void commonSettingsStorage(SettingsStorage commonSettingsStorage);

  SettingsStorage reportsUserSettingsStorage();

  void reportsUserSettingsStorage(SettingsStorage reportsUserSettingsStorage);

  SettingsStorage reportsVariantsStorage();

  void reportsVariantsStorage(SettingsStorage reportsVariantsStorage);

  SettingsStorage formDataSettingsStorage();

  void formDataSettingsStorage(SettingsStorage formDataSettingsStorage);

  SettingsStorage dynamicListsUserSettingsStorage();

  void dynamicListsUserSettingsStorage(SettingsStorage dynamicListsUserSettingsStorage);

  CommonForm defaultReportForm();

  void defaultReportForm(CommonForm defaultReportForm);

  CommonForm defaultReportVariantForm();

  void defaultReportVariantForm(CommonForm defaultReportVariantForm);

  CommonForm defaultReportSettingsForm();

  void defaultReportSettingsForm(CommonForm defaultReportSettingsForm);

  CommonForm defaultDynamicListSettingsForm();

  void defaultDynamicListSettingsForm(CommonForm defaultDynamicListSettingsForm);

  CommonForm defaultSearchForm();

  void defaultSearchForm(CommonForm defaultSearchForm);

  CommonForm defaultDataHistoryChangeHistoryForm();

  void defaultDataHistoryChangeHistoryForm(CommonForm defaultDataHistoryChangeHistoryForm);

  CommonForm defaultDataHistoryVersionDataForm();

  void defaultDataHistoryVersionDataForm(CommonForm defaultDataHistoryVersionDataForm);

  CommonForm defaultDataHistoryVersionDifferencesForm();

  void defaultDataHistoryVersionDifferencesForm(CommonForm defaultDataHistoryVersionDifferencesForm);

  CommonForm defaultCollaborationSystemUsersChoiceForm();

  void defaultCollaborationSystemUsersChoiceForm(CommonForm defaultCollaborationSystemUsersChoiceForm);

  List<RequiredMobileApplicationPermissions> requiredMobileApplicationPermissions();

  Language defaultLanguage();

  void defaultLanguage(Language defaultLanguage);

  MultiLanguageText briefInformation();

  void briefInformation(MultiLanguageText briefInformation);

  MultiLanguageText detailedInformation();

  void detailedInformation(MultiLanguageText detailedInformation);

  MultiLanguageText copyright();

  void copyright(MultiLanguageText copyright);

  MultiLanguageText vendorInformationAddress();

  void vendorInformationAddress(MultiLanguageText vendorInformationAddress);

  MultiLanguageText configurationInformationAddress();

  void configurationInformationAddress(MultiLanguageText configurationInformationAddress);

  MainClientApplicationWindowMode mainClientApplicationWindowMode();

  void mainClientApplicationWindowMode(MainClientApplicationWindowMode mainClientApplicationWindowMode);

  DataLockControlMode dataLockControlMode();

  void dataLockControlMode(DataLockControlMode dataLockControlMode);

  ObjectAutoNumerationMode objectAutoNumerationMode();

  void objectAutoNumerationMode(ObjectAutoNumerationMode objectAutoNumerationMode);

  ModalityUseMode modalityUseMode();

  void modalityUseMode(ModalityUseMode modalityUseMode);

  SynchronousPlatformExtensionAndAddInCallUseMode synchronousPlatformExtensionAndAddInCallUseMode();

  void synchronousPlatformExtensionAndAddInCallUseMode(SynchronousPlatformExtensionAndAddInCallUseMode synchronousPlatformExtensionAndAddInCallUseMode);

  SynchronousPlatformExtensionAndAddInCallUseMode synchronousExtensionAndAddInCallUseMode();

  void synchronousExtensionAndAddInCallUseMode(SynchronousPlatformExtensionAndAddInCallUseMode synchronousExtensionAndAddInCallUseMode);

  InterfaceCompatibilityMode interfaceCompatibilityMode();

  void interfaceCompatibilityMode(InterfaceCompatibilityMode interfaceCompatibilityMode);

  CompatibilityMode compatibilityMode();

  void compatibilityMode(CompatibilityMode compatibilityMode);

  CommonForm defaultConstantsForm();

  void defaultConstantsForm(CommonForm defaultConstantsForm);

  UsedFunctionality usedMobileApplicationFunctionalities();

  void usedMobileApplicationFunctionalities(UsedFunctionality usedMobileApplicationFunctionalities);

  Picture mainSectionPicture();

  void mainSectionPicture(Picture mainSectionPicture);

  Picture splash();

  void splash(Picture picture);

  Style defaultStyle();

  void defaultStyle(Style style);


//  List<Language> languages();
//
//  List<Subsystem> subsystems();
//
//  List<StyleItem> styleItems();
//
//  List<Style> styles();
//
//  List<Catalog> catalogs();
//
//  List<Picture> commonPictures();
//
//  List<V8Interface> interfaces();
//
//  List<SessionParameter> sessionParameters();
//
//  List<Role> roles();
//
//  List<CommonTemplate> commonTemplates();
//
//  List<FilterCriteria> filterCriteria();
//
//  List<CommonModule> commonModules();
//
//  List<CommonAttribute> commonAttributes();
//
//  List<ExchangePlan> exchangePlans();
//
//  List<XdtoPackage> xdtoPackages();
//
//  List<WebService> webServices();
//
//  List<HttpService> httpServices();
//
//  List<WsReference> wsReferences();
//
//  List<EventSubscription> eventSubscriptions();
//
//  List<ScheduledJob> scheduledJobs();
//
//  List<SettingsStorage> settingsStorages();
//
//  List<FunctionalOption> functionalOptions();
//
//  List<FunctionalOptionParameter> functionalOptionParameters();
//
//  List<DefinedType> definedTypes();
//
//  List<CommonCommand> commonCommands();
//
//  List<CommandGroup> commandGroups();
//
//  List<Constant> constants();
//
//  List<CommonForm> commonForms();
//
//  List<Document> documents();
//
//  List<DocumentNumerator> documentNumerators();
//
//  List<Sequence> sequences();
//
//  List<DocumentJournal> documentJournals();
//
//  List<V8Enum> enums();
//
//  List<Report> reports();
//
//  List<DataProcessor> dataProcessors();
//
//  List<InformationRegister> informationRegisters();
//
//  List<AccumulationRegister> accumulationRegisters();
//
//  List<ChartOfCharacteristicTypes> chartsOfCharacteristicTypes();
//
//  List<ChartOfAccounts> chartsOfAccounts();
//
//  List<AccountingRegister> accountingRegisters();
//
//  List<ChartOfCalculationTypes> chartsOfCalculationTypes();
//
//  List<CalculationRegister> calculationRegisters();
//
//  List<BusinessProcess> businessProcesses();
//
//  List<Task> tasks();
//
//  List<ExternalDataSource> externalDataSources();
//
//  List<IntegrationService> integrationServices();
//
//  List<Bot> bots();
}
