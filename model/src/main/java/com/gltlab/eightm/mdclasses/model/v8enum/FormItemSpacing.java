/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.v8enum;

public enum FormItemSpacing {
  /**
   * Default value
   */
  AUTO,
  DOUBLE,
  HALF,
  NONE,
  ONE_AND_HALF,
  SINGLE;

  public static FormItemSpacing getFormItemSpacingByString(String value) {
    return switch (value) {
      case "Auto" -> AUTO;
      case "Double" -> DOUBLE;
      case "Half" -> HALF;
      case "None" -> NONE;
      case "OneAndHalf" -> ONE_AND_HALF;
      case "Single" -> SINGLE;
      default -> throw new IllegalArgumentException("Unknown FormItemSpacing value \"" + value + "\"");
    };
  }
}
