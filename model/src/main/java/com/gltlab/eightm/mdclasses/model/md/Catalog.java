/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.md;

import com.gltlab.eightm.mdclasses.model.forms.CatalogForm;
import com.gltlab.eightm.mdclasses.model.md.children.CatalogTypes;
import com.gltlab.eightm.mdclasses.model.v8enum.AllowedLength;
import com.gltlab.eightm.mdclasses.model.v8enum.CatalogCodeType;
import com.gltlab.eightm.mdclasses.model.v8enum.CatalogMainPresentation;
import com.gltlab.eightm.mdclasses.model.v8enum.ChoiceMode;
import com.gltlab.eightm.mdclasses.model.v8enum.EditType;
import com.gltlab.eightm.mdclasses.model.v8enum.HierarchyType;
import com.gltlab.eightm.mdclasses.model.v8enum.SubordinationUse;

import java.util.List;

public interface Catalog extends CatalogOwner {

  CatalogTypes producedTypes();

  void producedTypes(CatalogTypes producedTypes);

  boolean hierarchical();

  void hierarchical(boolean hierarchical);

  HierarchyType hierarchyType();

  void hierarchyType(HierarchyType hierarchyType);

  boolean limitLevelCount();

  void limitLevelCount(boolean limitLevelCount);

  int levelCount();

  void levelCount(int levelCount);

  boolean foldersOnTop();

  void foldersOnTop(boolean foldersOnTop);

  List<CatalogOwner> owners();

  SubordinationUse subordinationUse();

  void subordinationUse(SubordinationUse subordinationUse);

  int codeLength();

  void codeLength(int codeLength);

  int descriptionLength();

  void descriptionLength(int descriptionLength);

  List<CatalogForm> forms();

  CatalogForm defaultObjectForm();

  void defaultObjectForm(CatalogForm defaultObjectForm);

  CatalogForm defaultFolderForm();

  void defaultFolderForm(CatalogForm defaultFolderForm);

  CatalogForm defaultListForm();

  void defaultListForm(CatalogForm defaultListForm);

  CatalogForm defaultChoiceForm();

  void defaultChoiceForm(CatalogForm defaultChoiceForm);

  CatalogForm defaultFolderChoiceForm();

  void defaultFolderChoiceForm(CatalogForm defaultFolderChoiceForm);

  CatalogCodeType codeType();

  void codeType(CatalogCodeType codeType);

  AllowedLength codeAllowedLength();

  void codeAllowedLength(AllowedLength codeAllowedLength);

  boolean checkUnique();

  void checkUnique(boolean checkUnique);

  boolean autoNumbering();

  void autoNumbering(boolean autoNumbering);

  CatalogMainPresentation defaultPresentation();

  void defaultPresentation(CatalogMainPresentation defaultPresentation);

  EditType editType();

  void editType(EditType editType);

  ChoiceMode choiceMode();

  void choiceMode(ChoiceMode choiceMode);
}
