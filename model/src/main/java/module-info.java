module mdclasses4j.model {
  requires jakarta.xml.bind;

  exports com.gltlab.eightm.mdclasses.model.base;
  exports com.gltlab.eightm.mdclasses.model.forms;
  exports com.gltlab.eightm.mdclasses.model.forms.element;
  exports com.gltlab.eightm.mdclasses.model.forms.ext;
  exports com.gltlab.eightm.mdclasses.model.help;
  exports com.gltlab.eightm.mdclasses.model.v8enum;
  exports com.gltlab.eightm.mdclasses.model.input;
  exports com.gltlab.eightm.mdclasses.model.forms.path;
  exports com.gltlab.eightm.mdclasses.model.forms.base;
  exports com.gltlab.eightm.mdclasses.model.md;
  exports com.gltlab.eightm.mdclasses.model;
  exports com.gltlab.eightm.mdclasses.model.modules;
  exports com.gltlab.eightm.mdclasses.model.md.children;
  exports com.gltlab.eightm.mdclasses.model.md.children.produced;
}